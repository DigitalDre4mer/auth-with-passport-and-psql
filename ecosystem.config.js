module.exports = {
  apps: [
    {
      name: "app",
      script: "./app.js",
      env: {
        NODE_ENV: "development"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ],
  deploy: {
    production: {
      user : 'deploy',
      host : '144.91.68.160',
      ref  : 'origin/master',
      repo : ' https://DigitalDre4mer@bitbucket.org/DigitalDre4mer/auth-with-passport-and-psql.git',
      "pre-setup" : "pwd",
      "post-deploy":"git update",
      "post-setup": "ls -la",
      path:"/var/www/office",
      ssh_options: "StrictHostKeyChecking=no",
      "post-deploy":
          "ls -la && pwd && npm install && npm run dev && pm2 reload ecosystem.config.js --env production && pm2 save"
    }
  }
};