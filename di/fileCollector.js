const Container = require('./container');

const container = new Container();

//configs
container.register('mainConfig', require('../configs/mainConfig'));
container.singleton('db', require('../configs/dbAdapter'),['mainConfig']);

//middlewares
container.register('auth', require('../middlewares/auth'));
container.register('index', require('../middlewares/index'));

//models
container.register('usersModel', require('../models/users'), ['db']);
container.register('authModel', require('../models/auth'), ['db']);

//misc
// container.register('helperFunctions', require('../helpers/miscFunctions'),['mainConfig']);

module.exports = container;