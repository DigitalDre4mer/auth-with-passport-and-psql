let md5 = require('md5');
module.exports = {
    doLogin(db, username, password) {
        const sql = `select id from users where username=$1 and password=$2`;
        const params = [username, md5(password)];
        return db.query(sql, params);
    },
    doSignUp(db, username, password) {
        const sql = `INSERT INTO users(username,password) values($1,$2)`;
        const params = [username, md5(password)];
        return db.query(sql, params);
    }
}