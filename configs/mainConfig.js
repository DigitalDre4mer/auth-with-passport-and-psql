/**
 * Main config file for backend functionality
 */
module.exports = {
    name:'Back Office',
    version: '0.01',
    port: 8100,
    salt:'7fa73b47df808d36c5fe328546ddef8b9011b2c6',
    secret:'HanSoloShotFirst',

    db: {
        host: 'localhost',
        port: '',
        database: 'beerTests',
        user: 'postgres',
        password: ''
    },
};