/*
* Set up the connection to the DB
* We have 2 main ideas here:
* 1)query is used when we require info
* 2)queryWrite is when we make any kind of modification on the db
*
* Please use them with caution!
* */

const { Pool } = require('pg');
const mainConfig = require('../configs/mainConfig');

const pool = new Pool(mainConfig.db);

module.exports = {
    query: (text, params, callback) => {
        if(pool.totalCount === 100) {
            notifier('Warning!', 'Please try again later!');
            throw 'Max pool connections reached!';
        }

        return pool.query(text, params, callback)
    },
};