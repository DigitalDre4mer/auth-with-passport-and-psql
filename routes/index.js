const routes = require('express').Router();
const container = require('../di/fileCollector');
const db = container.get('db');
const config = container.get('mainConfig');
const checkAuth = require('../middlewares/auth')(config);

routes.get('/', (req, res) => { res.status(200).json({ message: 'Connected!' }); });

routes.use('/auth', require('./auth.js')(db, config));
routes.use('/users', require('./users.js')(db));


module.exports = routes;